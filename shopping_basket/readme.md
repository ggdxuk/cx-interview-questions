# Shopping Basket Test

## Running

I have developed this as a CLI tool to keep it simple. To run, simply `python src/main.py [products]`. You can add as many or as few products as you want, for example `python src/main.py biscuits biscuits baked_beans baked_beans baked_beans`

The registered products are:

- baked_beans
- biscuits
- shampoo_small
- shampoo_medium
- shampoo_large

Unregistered products are handled too, try "bread" for example.

This application requires no third party libraries except for the "dev" requirements, pretty standard stuff like Pytest and Black.

## How it works

A Basket object is created immediately with an item property. You can add items at any point using the `Basket.add_item()` passing in an object that inherits from the `Product` class. You can create as many products as you like, either with or without discounts.

Discounts are objects that extend an abstract base class `Discount`, where you can configure whatever discount you want. Simply add a reference to the `discount_mapping` dictionary in `src/discount.py` and reference it in a product's `discount` property to enable it's use.

## Testing and linting

Run `make` to run Flake8, Black, Mypy and Pytest. If you'd like to run each individually:

`make format` for Black (Auto formatting)

`make type-check` for Mypy (Type checking)

`make pep8` for Flake8 (PEP8 conformity)

`make test` for Pytest (Unit tests)
