import json
from src.product import product_mapping


def initialise_product_config():
    products = json.load(open("src/data/products.json"))

    for product in products:
        cls = product_mapping[product["name"]]
        cls.cost = product["cost"]
        cls.discount = product["discount"]
