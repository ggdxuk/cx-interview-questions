from shopping_basket_tests.utils import initialise_product_config
from src.discount import BuyTwoGetOneFree, TwentyFivePercentOff
from src.product import BakedBeans

initialise_product_config()


def describe_buy_two_get_one_free():
    def test_discount_for_three_items_reduces_by_one():
        product = BakedBeans()

        product.quantity = 3

        discount = BuyTwoGetOneFree(product=product)
        assert discount.calculate_discount() == 0.99

    def test_discount_for_five_items_reduces_by_one():
        product = BakedBeans()

        product.quantity = 5

        discount = BuyTwoGetOneFree(product=product)

        assert discount.calculate_discount() == 0.99

    def test_discount_for_six_items_reduces_by_two():
        product = BakedBeans()

        product.quantity = 6

        discount = BuyTwoGetOneFree(product=product)

        assert discount.calculate_discount() == 1.98

    def test_discount_for_eighteen_items_reduces_by_six():
        product = BakedBeans()

        product.quantity = 18

        discount = BuyTwoGetOneFree(product=product)

        assert discount.calculate_discount() == 5.94


def describe_twenty_five_percent_off():
    def test_discount():
        product = BakedBeans()

        product.quantity = 3

        discount = TwentyFivePercentOff(product=product)
        assert discount.calculate_discount() == 0.74
