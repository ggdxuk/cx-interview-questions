from shopping_basket_tests.utils import initialise_product_config
from src.basket import Basket
from src.product import (
    BakedBeans,
    Biscuits,
    Product,
    ShampooMedium,
    ShampooSmall,
)


class NegativelyPricedProduct(Product):
    cost = -0.99
    name = "Bad Beans"
    discount = None


def describe_basket():
    initialise_product_config()

    def test_price_always_positive_or_zero():
        basket = Basket()
        assert basket.price == 0

        negatively_priced_product = NegativelyPricedProduct()
        basket.items.append(negatively_priced_product)
        assert basket.price == 0

        positively_priced_product = Biscuits()
        basket.items.append(positively_priced_product)
        assert basket.price == 0.21

    def test_add_different_products_to_basket():
        basket = Basket()

        products = [
            BakedBeans(),
            BakedBeans(),
        ]
        for product in products:
            basket.add_item(product)

        assert len(basket.items) == 1

    def test_add_multiple_same_products_to_basket():
        basket = Basket()

        products = [
            BakedBeans(),
            BakedBeans(),
        ]
        for product in products:
            basket.add_item(product)

        assert len(basket.items) == 1
        assert basket.items[0].quantity == 2

    def test_multiple_same_products_in_basket_price():
        basket = Basket()

        products = [
            BakedBeans(),
            BakedBeans(),
        ]
        for product in products:
            basket.add_item(product)

        assert basket.price == 1.98

    def test_total_discount():
        basket = Basket()

        products = [
            BakedBeans(),
            BakedBeans(),
            BakedBeans(),
            ShampooSmall(),
            ShampooMedium(),
            Biscuits(),
            Biscuits(),
        ]

        for product in products:
            basket.add_item(product)

        # 2x biscuits at 25% off = 0.60
        # 3x beans at buy two get one free = 0.99
        assert basket.total_discount == 1.59

    def test_totals():
        basket = Basket()

        products = [
            BakedBeans(),
            BakedBeans(),
            BakedBeans(),
            ShampooSmall(),
            ShampooMedium(),
            Biscuits(),
            Biscuits(),
        ]

        for product in products:
            basket.add_item(product)

        assert basket.totals["sub-total"] == 9.87
        assert basket.totals["discount"] == 1.59
        assert basket.totals["total"] == 8.28
