import json
from typing import Optional


class Product:
    name: str
    cost: float
    discount: Optional[str]
    quantity: int = 1


class BakedBeans(Product):
    name = "Baked Beans"
    cost: float = 0.00
    discount: Optional[str] = None


class Biscuits(Product):
    name = "Biscuits"
    cost: float = 0.00
    discount: Optional[str] = None


class ShampooSmall(Product):
    name = "Shampoo (Small)"
    cost: float = 0.00
    discount: Optional[str] = None


class ShampooMedium(Product):
    name = "Shampoo (Medium)"
    cost: float = 0.00
    discount: Optional[str] = None


class ShampooLarge(Product):
    name = "Shampoo (Large)"
    cost: float = 0.00
    discount: Optional[str] = None


product_mapping = {
    "baked_beans": BakedBeans,
    "biscuits": Biscuits,
    "shampoo_small": ShampooSmall,
    "shampoo_medium": ShampooMedium,
    "shampoo_large": ShampooLarge,
}


def initialise_product_config():
    products = json.load(open("data/products.json"))

    for product in products:
        cls = product_mapping[product["name"]]
        cls.cost = product["cost"]
        cls.discount = product["discount"]
