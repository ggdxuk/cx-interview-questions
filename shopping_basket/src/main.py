import sys
from shopping_basket_tests.utils import initialise_product_config
from src.basket import Basket
from src.product import product_mapping


def main():

    initialise_product_config()

    basket = Basket()

    if len(sys.argv) >= 1:
        products = sys.argv[1:]

        for product in products:
            try:
                basket.add_item(product_mapping[product]())
            except KeyError:
                print(f"Sorry, we are out of stock of {product}")

    print(basket.totals)


if __name__ == "__main__":
    main()
