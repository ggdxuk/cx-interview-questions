from abc import ABC, abstractmethod
from math import ceil
from src.product import Product


class Discount(ABC):
    def __init__(self, product: Product):
        self.product = product
        super(Discount, self).__init__()

    @abstractmethod
    def calculate_discount(self) -> float:
        pass


class BuyTwoGetOneFree(Discount):
    def calculate_discount(self) -> float:
        chargeable = ceil(self.product.quantity * 2 / (2 + 1))
        discount = (self.product.quantity - chargeable) * self.product.cost

        return round(discount, 2)


class TwentyFivePercentOff(Discount):
    def calculate_discount(self) -> float:
        discount = (self.product.quantity * self.product.cost) * 0.25

        return round(discount, 2)


discount_mapping = {
    "buy_two_get_one_free": BuyTwoGetOneFree,
    "twenty_five_off": TwentyFivePercentOff,
}
