from typing import List
from src.discount import discount_mapping
from src.product import Product


class Basket:
    items: List[Product]

    def __init__(self):
        self.items = []

    def add_item(self, item: Product) -> None:
        if not any(product.name == item.name for product in self.items):
            self.items.append(item)
        else:
            for basket_item in self.items:
                if basket_item.name == item.name:
                    basket_item.quantity = basket_item.quantity + 1
                    break

    @property
    def price(self) -> float:
        total_price = sum([x.cost * x.quantity for x in self.items])

        return round(total_price, 2) if total_price >= 0 else 0

    @property
    def total_discount(self) -> float:
        discount = sum(
            [
                discount_mapping[item.discount](item).calculate_discount()
                for item in self.items
                if item.discount is not None
            ]
        )

        return round(discount, 2)

    @property
    def totals(self) -> dict:
        return {
            "sub-total": self.price,
            "discount": self.total_discount,
            "total": round(self.price - self.total_discount, 2),
        }
